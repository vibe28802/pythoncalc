from calc3 import *
# Initialize Logger and Calculator
logger = Logger()
calculator = Calculator(logger)

# Start the calculator
calculator.calculate()
