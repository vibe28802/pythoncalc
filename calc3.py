import logging

class Logger: #Logger class til simpel mellemregning/resultat logning
    def __init__(self, filename='example.log', level=logging.DEBUG): #Navn på log fil og log level
        logging.basicConfig(filename=filename, level=level)

    def log_debug(self, message):
        logging.debug(message)


class Calculator: #Calc class
    def __init__(self, logger):
        self.logger = logger
        self.operations = {
            '+': ('added to', self.add),
            '-': ('subtracted from', self.sub),
            '/': ('divided by', self.div),
            'x': ('multiplied by', self.mult)
        }

    def add(self, num1, num2):
        return num1 + num2

    def sub(self, num1, num2):
        return num1 - num2

    def div(self, num1, num2):
        return num1 / num2

    def mult(self, num1, num2):
        return num1 * num2

    def calculate(self):
        print('\nWelcome to the simple calculator (write q to quit)')
        while True:
            user_input = input("Enter 'num1 operation num2' (or q to quit): ").split() #Split input op ved whitespaces til en liste
            
            if 'q' in user_input:
                print('Goodbye...')
                break

            if len(user_input) != 3 or user_input[1] not in self.operations: #Check om listen af input er valid
                print("Invalid format or operation. Please use the format 'num1 operation num2'.")
                continue

            self.logger.log_debug(f'User input: {user_input}') # User inpout loggging

            number1, operation, number2 = user_input

            try:
                number1, number2 = float(number1), float(number2)
                operation_name, operation_func = self.operations[operation]
                result = operation_func(number1, number2)
                print(f'{number1} {operation_name} {number2} = {result}')
                self.logger.log_debug(f'User result: {result}') #result logging

            except ValueError:
                print('Invalid input. Please enter two numbers and an operation.')
            except ZeroDivisionError:
                print('Cannot divide by zero, please try again')


# Initialize Logger and Calculator
logger = Logger()
calculator = Calculator(logger)

# Start the calculator
calculator.calculate()
