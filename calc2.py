import logging
logging.basicConfig(filename='example.log', level=logging.DEBUG)


def add(num1, num2):
    return num1 + num2

def sub(num1, num2):
    return num1 - num2

def div(num1, num2):
    return num1 / num2

def mult(num1, num2):
    return num1 * num2

operations = {
    '+': ('added to', add),
    '-': ('subtracted from', sub),
    '/': ('divided by', div),
    'x': ('multiplied by', mult)
}

print('\nWelcome to the simple calculator (write q to quit)')

while True:     #Main loop
    user_input = input("Enter 'num1 operation num2' (or q to quit): ").split() #split bruge input op ved white spaces
    
    if 'q' in user_input:
        print('Goodbye...')
        break

    if len(user_input) != 3 or user_input[1] not in operations.keys():      #Check om bruger input er 3 strings, fortsæt kun hvis 3 
        print("Invalid format or operation. Please use the format 'num1 operation num2' where operation is one of 1 (add), 2 (sub), 3 (div), 4 (mult).")
        continue
    logging.debug('User input: %s', user_input)

    number1, operation, number2 = user_input #indsæt bruger  input 

    try:
        number1, number2 = float(number1), float(number2)
        operation_name, operation_func = operations[operation]
        result = operation_func(number1, number2)
        print(f'{number1} {operation_name} {number2} = {result}')
        logging.debug('User result: %s', result)

    except ValueError:
        print('Invalid input. Please enter two numbers and an operation.')
    except ZeroDivisionError:
        print('Cannot divide by zero, please try again')
